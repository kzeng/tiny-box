#include <stdio.h>
#include <stdlib.h>
#include <Adafruit_GFX.h>
#include <Adafruit_SSD1306.h>
#include <ESP8266WiFi.h>

#include <NTPClient.h>
#include <WiFiUdp.h>
 
#include <dht11.h>//引入DHT11库
 
/********************###定义###********************/
dht11 DHT11;//定义传感器类型
#define DHT11PIN 2//定义传感器连接引脚。此处的PIN2在NodeMcu8266开发板上对应的引脚是D4

const char* ssid     = "Openwrt-fast"; 
const char* password = "zk010823";   


Adafruit_SSD1306 display(128, 64, &Wire, -1);

WiFiUDP ntpUDP;
NTPClient timeClient(ntpUDP, "ntp.aliyun.com"); //NTP地址
 
void setup() {
  Serial.begin(115200); 

  Serial.println("\n");
  Serial.println("esp8266读取DHT11传感器数值 ");
  Serial.print("DHT11库文件版本: ");
  Serial.println(DHT11LIB_VERSION);

  WiFi.mode(WIFI_STA);
  WiFi.begin(ssid, password);

  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }

  Serial.println("");
  Serial.println("WiFi connected");
  Serial.println("IP address: ");
  Serial.println(WiFi.localIP());

  timeClient.begin();
  timeClient.setTimeOffset(28800); //+1区，偏移3600，+8区，偏移3600*8

  if (!display.begin(SSD1306_SWITCHCAPVCC, 0x3C)) {
    Serial.println(F("SSD1306 allocation failed"));
    for (;;);
  }
  delay(2000);
  display.clearDisplay();
  display.setTextColor(WHITE);
}

int flag = 1;


void loop() {
  Serial.println(millis());
  timeClient.update();

  unsigned long epochTime = timeClient.getEpochTime();
  Serial.print("Epoch Time:");
  Serial.println(epochTime);
 
  //打印时间
  int currentHour = timeClient.getHours();
  Serial.print("Hour:");
  Serial.println(currentHour);
  int currentMinute = timeClient.getMinutes();
  Serial.print("Minute:");
  Serial.println(currentMinute);
  int currentSecond = timeClient.getSeconds();
  Serial.print("Seconds:");
  Serial.println(currentSecond);

  //将epochTime换算成年月日
  struct tm *ptm = gmtime((time_t *)&epochTime);
  int year = ptm->tm_year;
  Serial.print("Year:");
  Serial.println(year+1900);
  int monthDay = ptm->tm_mday;
  Serial.print("Month day:");
  Serial.println(monthDay);
  int currentMonth = ptm->tm_mon + 1;
  Serial.print("Month:");
  Serial.println(currentMonth);


  // 3秒刷新显示内容
  delay(2000);
  // 清空显示
  display.clearDisplay();
  // 使更改的显示生效
  display.display();

  // 判断应该显示的内容
  flag = flag % 2;

  if (flag == 1) {

    display.setTextSize(1);  // 设置字体大小（1~8）
    display.setCursor(0, 0); // 设置坐标
    display.print("Time:"); // 显示内容
    display.setTextSize(2);
    display.setCursor(0, 15);
    // display.print("2022-03-18 21:51:18");
    display.print(year+1900);
    display.print("-");
    display.print(currentMonth);
    display.print("-");
    display.print(monthDay);

    display.setCursor(0, 40);
    display.print(currentHour);
    display.print(":");
    display.print(currentMinute);
    display.print(":");
    display.print(currentSecond);

  } 
  
  
  if (flag == 0) {


  DHT11.read(DHT11PIN); //更新传感器所有信息
 
  float humidity = (float)DHT11.humidity;
  float temperature = (float)DHT11.temperature;

    Serial.print("当前湿度 (%): ");
    Serial.println(humidity, 2);
    Serial.print("当前温度 (℃): ");
    Serial.println(temperature, 2);

    // show temp
    display.setTextSize(1);
    display.setCursor(0, 0);
    display.print("Temperature: ");
    display.setTextSize(2);
    display.setCursor(0, 15);
    // display.print(28.6);
    display.print(temperature);

    display.print(" ");
    display.setTextSize(1);
    display.cp437(true);
    display.write(167);
    display.setTextSize(2);
    display.print("C");

    // show humidity
    display.setTextSize(1);
    display.setCursor(0, 0+35);
    display.print("Humidity: ");
    display.setTextSize(2);
    display.setCursor(0, 15+35);
    // display.print(46);
    display.print(humidity);
    display.print(" %");

    // delay(1000);
  }

  flag++;

  display.display();
}


