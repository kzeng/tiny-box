# TINY-BOX

### 介绍
项目代号：小盒子。基于物联网应用的、开箱即用的、温湿度智能监控软硬件一体化产品与解决方案。


### 需求
开箱即用，配置简单。

* 上电后智能配网
* OLED显示当前温湿度和日志信息
* 实时监测环境温湿度
* 数据上云
* 通过红外自动温控
* 可手机端APP远程监控
* 自动远程维护


### 硬件

#### 材料清单
|器件        |型号               |备注              |
|-----------|-------------------|-----------------|
| ESP8266   | ESP8266           | NodeMCU模组      |
| IR        | -                 | 一组红外收发器件   |
| DHT11     | DHT11             | 温湿度传感器        |
| AM2301A     | AM2301A（DHT22）             | 温湿度传感器        |
| OLED96    | OLED96            | 0.96寸OLCD屏幕   |


#### 电路连接


|DHT11端     |ESP8266端    |
|------------|---------   |
| S(信号线)  | D4 (GPIO2)  | 
| +(电源)    | 3v3         | 
| -(电源)    | GND         | 


|AM2301A端（DHT22）    |ESP8266端    |
|------------|---------   |
| 黄 | D4 (GPIO2)    | 
| 红   | 3v3         | 
| 黑   | GND         | 


|OLED端      |ESP8266端     |
|------------|--------------|
| VCC        | 3.3V         | 
| GND        | GND          | 
| SCL        | D1           | 
| SDA        | D2           | 


|IR Sender     |ESP8266端     |
|------------|--------------|
| VCC        | 3.3V         | 
| GND        | GND          | 
| DATA        | D5 (GPIO14) | 



### 软件与开发环境

|软件与库        |版本            |备注              |
|---------------|---------------|-----------------|
| Arduion IDE   | 2.2.1         | 集成开发环境      |
| esp8266       | 3.1.2         | ESP8266库 by community   |
| IRremote      | 4.2.0         | 红外遥控库        |
| U8g2          | 2.34.22       | 0.96寸OLCD屏幕驱动库(没有用这个库)   |
| DHT11         | 2.0.0         | DHT11驱动库   |
| Adafruit SSD1306        | 2.6.7       | 0.96寸OLED96驱动库   |
| Adafruit GFX Library    | 1.11.8      | 0.96寸OLED96驱动库   |
| NTPClient               | 3.2.1       | NTPClient网络授时库   |
| IRremoteESP8266         | 2.8.6       | 红外遥控库        |


### 单元功能实现与测试

#### ESP8266-NodeMCU 

<img src="./docs/ESP8266-12-E-NodeMCU-Pinout.png" width=85%>


#### OLED.96 显示
***PASS***

#### DHT11 温湿度
***PASS***

#### WiFiManager 智能配网 
***PASS***


#### IRremote 红外遥控
***PASS***


#### 数据上云

#### 手机端APP控制
***PASS***


### 集成


### 系统测试


### 已知问题


### 参考资料

#### DHT11/DHT22
- https://blog.csdn.net/acktomas/article/details/117073078
- https://github.com/adafruit/DHT-sensor-library
  

#### OLED
- https://blog.csdn.net/huyuyang6688/article/details/124059607
- https://randomnerdtutorials.com/esp8266-0-96-inch-oled-display-with-arduino-ide/


#### IR

#### ESP8266/WiFiManager



### 软件


<img src="./docs/login.jpg" width=61%>

<img src="./docs/mqtt-iot-lite.jpg" width=85%>



#### mqtt 客户端设计说明

- 以配置文件驱动 .ini, .json, .yaml
- 基于web的技术
- PC 和移动端都适用， 也可打包为app
- 初始版实现以下功能
    - 配置页面：配置mqtt server, server address, port, protocol
    - UI 组件配置, 新增，修改，删除
    - 仪表盘页面 （UI 主页面，显示设备，传感器等的基本信息）
    - 内置组件库 (文本，进度条，仪表盘 ...)
- flask, paho-mqtt

### 配置格式示例

```javascript
{
    'server' : {
        'address': '192.168.1.100',
        'port': 1883
    },
    'component' :{
        'component_temp1': {
            'sub_name': 'sensor_temp',
            'min': 0,
            'max': 50,
            'value': 25,
            'color': '#ff0000',
            'size': 'large'
        },

    'component_humi1': {
            'sub_name': 'sensor_hmi',
            'min': 0,
            'max': 100,
            'value': 68,
            'color': '#0000ff',
            'size': 'large'
        },


    },

}
```



ESP8266在AP 模式下，通过手机进行配置，测试
<img src="./docs/Screenshot_2024-04-08-11-34-35.jpg" width=65%>


### 版本记录
